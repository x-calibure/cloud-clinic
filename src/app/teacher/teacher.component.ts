import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@app/_models';
import { UserService } from '@app/_services';


@Component({ templateUrl: './teacher.component.html' })
export class TeacherComponent implements OnInit {
  loading = false;
  user: User;

  constructor(private userService: UserService) { }

  ngOnInit() {
    //this.loading = true;
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }
}
