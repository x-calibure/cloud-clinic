﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { TeacherComponent } from './teacher/teacher.component';
import { AuthGuard } from './_helpers';

const routes: Routes = [
    { path: 'teacher', component: TeacherComponent, canActivate: [AuthGuard] },
    { path: 'admin', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);